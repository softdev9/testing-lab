<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>iframe_Change Password_f47ab0db9aa3c8</name>
   <tag></tag>
   <elementGuidId>90ab2bad-d18e-4f60-a878-3160c73c4f23</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//iframe[@name='f47ab0db9aa3c8']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>iframe[name=&quot;f47ab0db9aa3c8&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>iframe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>f47ab0db9aa3c8</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>1000px</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>1000px</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-testid</name>
      <type>Main</type>
      <value>dialog_iframe</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>frameborder</name>
      <type>Main</type>
      <value>0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allowtransparency</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allowfullscreen</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>scrolling</name>
      <type>Main</type>
      <value>no</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>allow</name>
      <type>Main</type>
      <value>encrypted-media</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://web.facebook.com/v12.0/plugins/customerchat.php?app_id=&amp;attribution=biz_inbox&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df1362e64df32534%26domain%3Dmyid.buu.ac.th%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fmyid.buu.ac.th%252Ff30aa75cc7b2d68%26relation%3Dparent.parent&amp;container_width=881&amp;current_url=https%3A%2F%2Fmyid.buu.ac.th%2Fprofile%2Fchgpwdlogin&amp;is_loaded_by_facade=true&amp;locale=th_TH&amp;log_id=5e36f2c7-465c-4b2e-a32e-1796fe2fbc83&amp;page_id=160067777401491&amp;request_time=1649267479437&amp;sdk=joey</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;fb-root&quot;)/div[@class=&quot;fb_iframe_widget fb_invisible_flow&quot;]/span[1]/iframe[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//iframe[@name='f47ab0db9aa3c8']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='fb-root']/div[2]/span/iframe</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//iframe</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//iframe[@name = 'f47ab0db9aa3c8' and @src = 'https://web.facebook.com/v12.0/plugins/customerchat.php?app_id=&amp;attribution=biz_inbox&amp;channel=https%3A%2F%2Fstaticxx.facebook.com%2Fx%2Fconnect%2Fxd_arbiter%2F%3Fversion%3D46%23cb%3Df1362e64df32534%26domain%3Dmyid.buu.ac.th%26is_canvas%3Dfalse%26origin%3Dhttps%253A%252F%252Fmyid.buu.ac.th%252Ff30aa75cc7b2d68%26relation%3Dparent.parent&amp;container_width=881&amp;current_url=https%3A%2F%2Fmyid.buu.ac.th%2Fprofile%2Fchgpwdlogin&amp;is_loaded_by_facade=true&amp;locale=th_TH&amp;log_id=5e36f2c7-465c-4b2e-a32e-1796fe2fbc83&amp;page_id=160067777401491&amp;request_time=1649267479437&amp;sdk=joey']</value>
   </webElementXpaths>
</WebElementEntity>
